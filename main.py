import os

from classes.Wallet import Wallet
from classes.Blocs import Blocs
from classes.Chain import Chain





def start():
    print('Pour tester le code plusieurs choix s\'offre a vous : ')
    print('Afficher tous les Wallets : 0')
    print('Créer un wallet : 1')
    print('Ajouter du solde a son wallet : 2')
    print('Enlever du solde à son Wallet : 3')
    print('Afficher son Wallet en renseignent son unique id : 4')
    print('Afficher tous les blocs existants : 5')
    print('Afficher un bloc en particulier : 6')
    print('Faire une transaction : 7')
    print('Avoir les détailles d\'une transation : 8')
    print('Avoir  le taille d\'un blocs : 9')
    print('')
    name = input("Entré un numéro pour l'action :")

    if isinstance(int(name), int):
        if name == '1':
            instance_wallet = Wallet()
            print("Wallet créer, il a pour identifiant → " + instance_wallet.unique_id)
            start()
        elif 0 <= int(name) < 5:
            wallet_test(name)
        elif 4 < int(name) < 8:
            chain_bloc_test(name)
        elif 7 < int(name):
            bloc_test(name)
    else:
        print("Votre valeur est incorrect \n")
        start()


def wallet_test(name):
    if name == "0":
        print(os.listdir('content/wallets/'))
        start()
    else:
        wallet = input('Entrer votre identifiant wallet ')

        if wallet != "":
            if os.path.exists('content/wallets/' + wallet + '.json'):
                instance_wallet = Wallet(wallet)
                if name == '2':
                    most_price = input("Entrer la somme à ajouter au compt : ")
                    instance_wallet.add_balance(int(most_price))
                    instance_wallet.save()
                    print("Votre compt à gagner " + str(most_price) + ", il est maintenant de " + str(
                        instance_wallet.balance))
                    start()
                elif name == '3':
                    less_price = input("Entrer la somme à enlever au compt : ")
                    instance_wallet.sub_balance(int(less_price))
                    instance_wallet.save()
                    print(
                        "Votre compt à perdu " + str(less_price) + ", il est maintenant de " + str(instance_wallet.balance))
                    start()
                elif name == '4':
                    instance_wallet.load(wallet)
                    data = {
                        'unique_id': instance_wallet.unique_id,
                        'balance': instance_wallet.balance,
                        'history': instance_wallet.history
                    }
                    print(data)
                    start()
            else:
                print('Wallet inconnu \n')
                print('Aller en créé un ! \n\n\n')
                start()

        else:
            wallet_test(name)


def chain_bloc_test(name, i=0):
    instance_chain = Chain()
    if name == '6':
        blocs = input("Rentrer le nom d'un blocs pour l'afficher : ")
        if os.path.exists('content/blocs/' + blocs + '.json'):
            result = instance_chain.get_block(blocs)
            print(result + "\n")
            start()
        else:
            print('Block inconnu \n')
            chain_bloc_test(name)
    elif name == '5':
        print(instance_chain.blocks)
        start()
    elif name == '7':
        i = i
        if i < 5:
            blocs_input = input("Blocs sauvegarde :")
            emmeteur = input("Wallet emetteur :")
            recepteur = input("Wallet recepteur :")
            montant = input("Montant de la transaction :")
            a = instance_chain.add_transaction(blocs_input, emmeteur, recepteur, int(montant))
            if not a:
                i += 1
                chain_bloc_test(name, i)
        else:
            print("Vous essayer une transaction trop de foix")


def bloc_test(name):
    hash_blocs = input("Entrer le nom d'un bloc :")
    instance_blocs = Blocs(hash_blocs)

    if instance_blocs.hash != "":
        if name == "8":
            trans = input('Code de votre transaction :')
            print(instance_blocs.get_transaction(trans))
            start()
        elif name == '9':
            print(instance_blocs.get_weight())
    else:
        start()

start()
