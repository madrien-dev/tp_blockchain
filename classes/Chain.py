import hashlib
import os
import random

from classes.Blocs import Blocs
from classes.Wallet import Wallet


class Chain:

    def __init__(self, lastTransaction=""):
        self.blocks = os.listdir('content/blocs/')
        self.last_transaction_number = lastTransaction

    def generate_hash(self):
        global hash
        valid_hash = False
        i = 88400
        while valid_hash == False:
            i += 1
            hash = hashlib.sha256(str(i).encode()).hexdigest()
            if self.verify_hash(str(hash)):
                valid_hash = True
        return hash, i

    def verify_hash(self, verify_hash):

        file_exist = os.path.exists('content/blocs/' + verify_hash + '.json')
        if file_exist:
            return False
        else:
            first = verify_hash[0:4]
            if first == '0000':
                return True
            else:
                return False

    def add_block(self):
        dir = os.listdir('content/blocs/')
        ale = random.randint(0, len(dir) - 1)
        result = self.generate_hash()
        instance_blocs = Blocs(result[0], str(result[1]), dir[ale].split('.')[0])
        return instance_blocs.save()

    @staticmethod
    def get_block(hash):
        instance_blocs = Blocs(hash)
        a = {
            "hash": instance_blocs.hash,
            "base_hash": instance_blocs.base_hash,
            "parent_hash": instance_blocs.parent_hash,
            "transactions": instance_blocs.transactions
        }
        return a

    def add_transaction(self, hash_block, hash_emetteur, hash_recepteur, montant):

        if os.path.exists('content/blocs/' + hash_block + '.json'):
            if os.path.exists('content/wallets/' + hash_emetteur + '.json'):
                if os.path.exists('content/wallets/' + hash_recepteur + '.json'):
                    instance_blocs = Blocs(hash_block)
                    instance_wallet = Wallet(hash_emetteur)
                    if instance_wallet.balance > montant:
                        if instance_blocs.get_weight() > 1000:
                            init = self.add_block()
                            instance_blocs.load(init)
                            instance_blocs.add_transaction(hash_emetteur, hash_recepteur, montant)
                            return True
                        else:
                            instance_blocs.add_transaction(hash_emetteur, hash_recepteur, montant)
                            return True
                    else:
                        print('solde insuffisant')
                        return False
                else:
                    print('recepteur inconnu')
                    return False
            else:
                print('emmetteur inconnu')
                return False
        else:
            print('Block inconnu')
            return False
